package com.nhamil.protInt.client.utils.Exceptions;

public class BadResponseException extends Exception {

    public BadResponseException(String message) {
        super(message);
    }
}
