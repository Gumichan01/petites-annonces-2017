package com.nhamil.protInt.client.utils.Exceptions;

public class CommandNotFoundException extends Exception {

    public CommandNotFoundException(String s) {
        super(s);
    }
}
